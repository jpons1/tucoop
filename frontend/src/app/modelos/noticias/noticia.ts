export interface Noticia {
    id: any;
    fecha_hora: string;
    titular: string;
    cuerpo: string;
    imagen_src: string;
}
