import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AccountService } from '../account-service.service';
import { AlertServiceService } from '../alert-service.service';

@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
  loading = false;
  submitted = false;
  usu: string = "";
  pass: string = "";
  usuario: any;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountService: AccountService,
    private alertService: AlertServiceService
  ) { }

  ngOnInit() {

  }

  // convenience getter for easy access to form fields

  onSubmit(): void {

    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    /*
    if (this.form.invalid) {
      return;
    }*/

    this.loading = true;
    this.accountService.login(this.usu, this.pass)
      .pipe(first())
      .subscribe({
        next: () => {
          this.accountService.user.subscribe(
            usu => this.usuario = usu
          );
          if (!this.usuario.token) {
            alert("Usuario incorrecto");
            this.alertService.error("Usuario incorrecto");
            this.loading = false;
          } else {
            const returnUrl = '/';
            this.router.navigateByUrl(returnUrl);
          }
          // get return url from query parameters or default to home page
        },
        error: (error: any) => {
          alert("Usuario incorrecto");
          this.alertService.error(error);
          this.loading = false;
        }
      });
  }
}