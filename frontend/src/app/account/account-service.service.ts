import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UrlAPI } from "../services/api.service";
import { User } from '../models/user';

//conexion a la api para loguearse

//observable: peticion a la api
@Injectable({ providedIn: 'root' })
export class AccountService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user') || '{}')); //cojo el user del localstorage, si no existe user ahi creas un array vacio
    this.user = this.userSubject.asObservable();
    console.log(this.user);
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  login(username: any, password: any) {
    //AQUI LA RUTA DE LA API
    return this.http.post<User>(UrlAPI.baseUrl+`user/authenticate`, { username, password })//verificar usuario
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        console.log(user);

        localStorage.setItem('user', JSON.stringify(user));
        this.userSubject.next(user);
        return user;

      }));
  }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('user');
    this.userSubject.next(new User());
    this.router.navigate(['/']);
  }
}