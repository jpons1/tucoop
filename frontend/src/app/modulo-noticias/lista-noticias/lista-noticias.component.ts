import { Component, OnInit } from '@angular/core';
import { NoticiasService } from '../../modulo-noticias/noticias.service';

@Component({
  selector: 'app-lista-noticias',
  templateUrl: './lista-noticias.component.html',
  styleUrls: ['./lista-noticias.component.scss']
})
export class ListaNoticiasComponent implements OnInit {

  constructor(private noticiasService: NoticiasService) { }
  //para sacar las dos ultimas noticias (las mas actuales)
  //SELECT * FROM Table ORDER BY ID DESC LIMIT 2
  noticias: any [] = [];

  ngOnInit(): void {
    this.noticiasService.getNoticias()
    .subscribe(
      data => {
        this.noticias = data;
        console.log('notiocias',this.noticias)
      },
      error=>{
        console.log(error)
      }
      );  
  
    console.log(this.noticias);
  }

}
