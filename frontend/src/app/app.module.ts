import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { JwtInterceptor } from './helpers/jwt.interceptor';

//IMPORTAR MODULOS
import { ModuloNoticiasModule } from './modulo-noticias/modulo-noticias.module';
import { ModuloServiciosModule } from './modulo-servicios/modulo-servicios.module';
import { ModuloHomeModule } from './modulo-home/modulo-home.module';
import { UsuarioModule } from './usuario/usuario.module';
import { AccountModule} from './account/account.module';

import { Menu2Component } from './menu2/menu2.component';
import { GestionhomeComponent } from './modulo-gestion/gestionhome/gestionhome.component';
import { GestiongastbenComponent } from './modulo-gestion/gestiongastben/gestiongastben.component';
import { ProductosComponent } from './modulo-gestion/productos/productos.component';
import { GestionserviciosComponent } from './modulo-gestion/gestionservicios/gestionservicios.component';
import { ResumenComponent } from './modulo-gestion/resumen/resumen.component';

import { ChartsModule } from 'ng2-charts';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    FooterComponent,
    Menu2Component,
    GestionhomeComponent,
    GestiongastbenComponent,
    ProductosComponent,
    GestionserviciosComponent,
    ResumenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ModuloNoticiasModule,
    ModuloServiciosModule,
    ModuloHomeModule,
    ChartsModule, 
    HttpClientModule,
    ReactiveFormsModule,
    AccountModule,
    FormsModule,
    UsuarioModule,
  ],
  exports:[
    FormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
