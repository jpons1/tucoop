import { Component, OnInit } from '@angular/core';
//import { Noticias } from "../../modelos/noticias/noticias";
import { Noticia } from "../../modelos/noticias/noticia";
import { NoticiasService } from '../../modulo-noticias/noticias.service';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.scss']
})
export class NoticiasComponent implements OnInit {
  //public noticia: Noticia = {id:"", fecha_hora: "", titular: "", cuerpo: "", imagen_src: "",};
  noticias:any []=[];
  constructor(private noticiasService: NoticiasService) { }

  /*noticias: any =[
    {
    'id' : 1,
    'titulo' : "noticia 1",
    'texto': 'explicacion noticia 1'
    },
    {'id' : 2,
    'titulo' : "noticia 2",
    'texto' : 'explicacion noticia 2'
    },
    {'id' : 3,
    'titulo' : "noticia 3",
    'texto' : 'explicacion noticia 3'
    }
  ]*/
  
  ngOnInit(): void {
    this.noticiasService.getNoticias()
    .subscribe(
      data => {
        this.noticias = data;
        console.log('notiocias',this.noticias)
      },
      error=>{
        console.log(error)
      }
      );  
  
    console.log(this.noticias);
  }

}
