import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaServiciosComponent } from './lista-servicios/lista-servicios.component';



@NgModule({
  declarations: [ListaServiciosComponent],
  imports: [
    CommonModule
  ],
  exports: [
    ListaServiciosComponent,
  ]
})
export class ModuloServiciosModule { }
