import { Injectable } from '@angular/core';
import { UrlAPI } from "../services/api.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class ServiciosService {
  constructor(private http: HttpClient) { }

  public getServicios(): Observable<any>{
  
    return this.http.get(UrlAPI.baseUrl+"servicios");
    
  }
}
