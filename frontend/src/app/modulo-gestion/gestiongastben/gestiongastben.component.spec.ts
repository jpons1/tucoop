import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestiongastbenComponent } from './gestiongastben.component';

describe('GestiongastbenComponent', () => {
  let component: GestiongastbenComponent;
  let fixture: ComponentFixture<GestiongastbenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestiongastbenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestiongastbenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
