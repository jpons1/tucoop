import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GestiongastbenComponent } from './gestiongastben/gestiongastben.component';
import { ProductosComponent } from './productos/productos.component';
import { GestionserviciosComponent } from './gestionservicios/gestionservicios.component';
import { ResumenComponent } from './resumen/resumen.component';
import { GestionhomeComponent } from './gestionhome/gestionhome.component';



@NgModule({
  declarations: [GestiongastbenComponent, ProductosComponent, GestionserviciosComponent, ResumenComponent, GestionhomeComponent],
  imports: [
    CommonModule
  ],
  exports : [
    GestiongastbenComponent,
    ProductosComponent,
    GestionserviciosComponent,
    ResumenComponent,
    GestionhomeComponent
  ]
})
export class ModuloGestionModule { }
