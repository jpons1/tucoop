import { Injectable } from '@angular/core';
import { UrlAPI } from "../services/api.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { AccountService } from '../account/account-service.service';

@Injectable({
  providedIn: 'root'
})
export class ServiceResumenService {
  usuario: any = {};
  constructor(private http: HttpClient, private AccountService: AccountService) { }

  public getNoticias(): Observable<any> {
    return this.http.get(UrlAPI.baseUrl + "productos-usuario");
  }
}
