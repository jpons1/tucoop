import { TestBed } from '@angular/core/testing';

import { ServiceResumenService } from './service-resumen.service';

describe('ServiceResumenService', () => {
  let service: ServiceResumenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceResumenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
