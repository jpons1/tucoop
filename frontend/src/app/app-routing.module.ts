import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//RUTAS
import { ListaNoticiasComponent } from './modulo-noticias/lista-noticias/lista-noticias.component';

import { ListaServiciosComponent } from './modulo-servicios/lista-servicios/lista-servicios.component';

import { NoticiasComponent } from './modulo-home/noticias/noticias.component';
import { ServiciosComponent } from './modulo-home/servicios/servicios.component';
import { HomeComponent } from './modulo-home/home/home.component';
import { ResumenComponent } from './modulo-gestion/resumen/resumen.component';


import {  } from './modulo-home/home/home.component';


import { LoginComponent } from './account/login/login.component';

import { PerfilPassComponent } from './usuario/perfil-pass/perfil-pass.component';
import { PerfilPrivacyComponent } from './usuario/perfil-privacy/perfil-privacy.component';
import { PerfilDPComponent } from './usuario/perfil-dp/perfil-dp.component';


import { ProductosComponent } from './modulo-gestion/productos/productos.component';
import { GestionserviciosComponent } from './modulo-gestion/gestionservicios/gestionservicios.component';
import { GestiongastbenComponent } from './modulo-gestion/gestiongastben/gestiongastben.component';
import { GestionhomeComponent} from './modulo-gestion/gestionhome/gestionhome.component';

const routes: Routes = [
 
  {
    path: 'listaNoticias',
    component: ListaNoticiasComponent
  },
  {
    path: 'listaServicios',
    component: ListaServiciosComponent
  },
  {
    path: 'resumenNoticias',
    component: NoticiasComponent
  },
  {
    path: 'resumenServicios',
    component: ServiciosComponent
  },
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'gestionhome',
    component: GestionhomeComponent  
  },
  {
    path: 'gestionproductos',
    component: ProductosComponent
  },
  {
    path: 'gestionservicios',
    component: GestionserviciosComponent
  },
  {
    path: 'gestiongastben',
    component:GestiongastbenComponent
  },
  {
    path: 'datospersonales',
    component: PerfilDPComponent
  },
  {
    path: 'privacidad',
    component: PerfilPrivacyComponent
  },
  {
    path: 'cambiarPass',
    component: PerfilPassComponent
  },
  {
    path: 'resumen',
    component: ResumenComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
