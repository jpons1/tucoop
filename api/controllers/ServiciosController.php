<?php

namespace app\controllers;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use app\models\Servicios;

class ServiciosController extends  ApiController
{
    public $modelClass = 'app\models\Servicios';//de que modelo tira el controlador
    
    public $authenable=false;//en este caso, aqui no interesa el token

    public function actions()
    {
        $actions = parent::actions();
        //Eliminamos acciones de crear y eliminar apuntes. Eliminamos update para personalizarla
        unset($actions['delete'], $actions['create'], $actions['update']);
        // Redefinimos el método que prepara los datos en el index
        $actions['index']['prepareDataProvider'] = [$this, 'indexProvider'];
        return $actions;
    }
    
    function indexProvider()
    {
        $cooperativa = $_GET['cooperativa']??"";
        if ($cooperativa!="") {
            return new ActiveDataProvider([
                'query' => Servicios::find()
                    ->where(['cooperativa_id' => $cooperativa])
            ]);
        } else {
            return new ActiveDataProvider([
                'query' => Servicios::find()

            ]);
        }
    }
}