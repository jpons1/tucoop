<?php
namespace app\controllers;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use app\models\Usuarios;

class UsuariosController extends  ApiController
{
    public function actions()
    {
        $actions = parent::actions();
        //Eliminamos acciones de crear y eliminar apuntes. Eliminamos update para personalizarla
        unset($actions['delete'], $actions['create'], $actions['update']);
        // Redefinimos el método que prepara los datos en el index
        $actions['index']['prepareDataProvider'] = [$this, 'indexProvider'];
        return $actions;
    }
    
    function indexProvider()
    {
        $cooperativa = $_GET['cooperativa']??"";
        if ($cooperativa!="") {
            return new ActiveDataProvider([
                'query' => Usuarios::find()
                    ->where(['cooperativa_id' => $cooperativa])
            ]);
        } else {
            return new ActiveDataProvider([
                'query' => Usuarios::find()

            ]);
        }
    }
    public $modelClass = 'app\models\Usuarios';

    public $authenable=false;
}