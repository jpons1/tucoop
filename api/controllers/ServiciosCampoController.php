<?php

namespace app\controllers;
use yii\filters\Cors;
use app\models\ServicioCampo;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;

class ServiciosCampoController extends  ApiController
{
    public $modelClass = 'app\models\ServicioCampo';
    

    public function actions()
    {
        $actions = parent::actions();
        //Eliminamos acciones de crear y eliminar apuntes. Eliminamos update para personalizarla
        unset($actions['delete'], $actions['create'], $actions['update']);
        // Redefinimos el método que prepara los datos en el index
        $actions['index']['prepareDataProvider'] = [$this, 'indexProvider'];
        return $actions;
    }

    public function indexProvider()
    {
        /*
        $usuario = $_GET["usuario"] ?? "";
        if($usuario == ""){
            return new ActiveDataProvider([
                'query' => ServicioCampo::find()
            ]);
        }else{
            return new ActiveDataProvider([
                'query' => ServicioCampo::find()
                ->where("campo_id IN (SELECT id FROM campo where usuario_id = $usuario)")
                ->orderBy("fecha_contrato")
            ]);
        }
        */
        $usu = $_GET["usuario"] ?? "";
        $cooperativa = ApiController::cooperativa_en_sesion()['id'] ?? "";
        if ($cooperativa != "" && $usu != "") {
            return new ActiveDataProvider([
                'query'=>ServicioCampo::find()
                ->where("campo_id IN (SELECT id FROM campo where usuario_id = $usu) and cooperativa_id
                 IN (SELECT cooperativa_id from campo where cooperativa_id=$cooperativa)")
                ->orderBy("fecha_contrato")
            ]);
        } else if ($usu != "") {
            return new ActiveDataProvider([
                'query' => ServicioCampo::find()
                ->select(["temporada_id", "producto_id", "sum(cantidad) as kilos", "fecha"])
                ->where("`campo_id` IN (SELECT campo_id FROM campo where usuario_id = $usu )")
            ]);
        }else {
            return new ActiveDataProvider([
                'query' => ServicioCampo::find()
            ]);
        }
        
    }


    public $authenable=false;
}
