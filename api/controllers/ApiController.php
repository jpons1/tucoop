<?php

namespace app\controllers;

use yii\filters\Cors;
use yii\filters\auth\HttpBearerAuth;
use yii\data\ActiveDataProvider;
use app\models\Noticias;
use Yii;

//el resto de controladores hereda de esta clase para que no falle el cors, y gestionar el token
class ApiController extends \yii\rest\ActiveController
{
    public $enableCsrfValidation = false;
    public $authenable = true;//en los controladores que no queramos el token se pone en false
    public $modelClass="d";

    public function beforeAction($a)
    {
        header('Access-Control-Allow-Origin: *');
        return parent::beforeAction($a);
    }

    public static function cooperativa_en_sesion()
    {

        try {
            $coop = $_GET['cooperativa'] ?? $_GET['coop']??"";
            if (isset(Yii::$app->session['cooperativa'])&&($coop == Yii::$app->session['cooperativa']['id'])) {
                return Yii::$app->session['cooperativa'];
            }else if ($coop != "" ) {
                Yii::$app->session['cooperativa'] = (
                    (new \yii\db\Query())
                    ->select(['id', 'nombre'])
                    ->from('cooperativa')
                    ->where(['id' => $coop])
                    ->all())[0];
                return Yii::$app->session['cooperativa'];
            } 
        } catch (\Throwable $th) {
            //throw $th;
            // die;
            //echo "exceptoide no identificADE";
            //die;
            return 0;
        }
        return 0;
    }


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'HEAD', 'OPTIONS', 'DELETE'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => $this->authenable,
                'Access-Control-Max-Age' => 86400
            ],
        ];

        if (!$this->authenable)
            return $behaviors;//behaviour->configuracion de la api(controladores)

        $behaviors['authenticator'] = [//token
            'class' => HttpBearerAuth::className(),
            'except' => ['options'],
        ];

        return $behaviors;
    }
}
