<?php

namespace app\controllers;

use Yii;
use app\models\Producto;
use yii\rest\ActiveController;
use app\models\ProductosUsuario;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\filters\auth\HttpBearerAuth;

class ProductosUsuarioController extends  ApiController
{
    public $modelClass = 'app\models\ProductosUsuario';

    public $authenable = false;

    public function actions()
    {
        $actions = parent::actions();
        //Eliminamos acciones de crear y eliminar apuntes. Eliminamos update para personalizarla
        unset($actions['delete'], $actions['create'], $actions['update']);
        // Redefinimos el método que prepara los datos en el index
        $actions['index']['prepareDataProvider'] = [$this, 'indexProvider'];
        return $actions;
    }

    public function indexProvider()
    {
        $usu = $_GET["usuario"] ?? "";
        $cooperativa = ApiController::cooperativa_en_sesion()['id'] ?? "";
        if ($cooperativa != "" && $usu != "") {
            return new ActiveDataProvider([
                'query'=>ProductosUsuario::find()->select([
                    "temporada_id", "beneficio",
                     "fecha", "producto_id", "usuario_id"])
                     ->where("usuario_id=$usu in
                     (select id from usuario where cooperativa_id=$cooperativa)"                   
                   )->groupBy(["producto_id", "temporada_id"])
            ]);
        } else if ($usu != "") {
            return new ActiveDataProvider([
                'query' => ProductosUsuario::find()->select(["temporada_id", "producto_id", "sum(cantidad) as kilos", "fecha"])->where('usuario_id=' . $usu)->groupBy(["producto_id", "temporada_id"])
            ]);
        } else if ($cooperativa != "") {
            echo "co";
            echo"<pre>";
            var_dump($cooperativa);
            die;
            return new ActiveDataProvider([
                'query' => ProductosUsuario::find()->select(["temporada_id", "producto_id", "sum(cantidad) as kilos", "fecha"])->where('cooperativa_id=' . $cooperativa)->groupBy(["producto_id", "temporada_id"])
            ]);
        } else {
            return new ActiveDataProvider([
                'query' => ProductosUsuario::find()->select(["temporada_id", "producto_id", "sum(cantidad) as kilos"])->groupBy(["producto_id", "temporada_id"])
            ]);
        }
    }
}
