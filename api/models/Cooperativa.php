<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cooperativa".
 *
 * @property int $id
 * @property string $nombre
 * @property string $direccion
 * @property int $cif
 */
class Cooperativa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cooperativa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nombre', 'direccion', 'cif'], 'required'],
            [['fecha_caducidad'],'date'],
            [['id', 'cif','limite_socios','precio'], 'integer'],
            [['nombre'], 'string', 'max' => 32],
            [['direccion'], 'string', 'max' => 128],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'cif' => 'Cif',
            'limite_socios' => 'Limite Socios',
            'precio' => 'Precio',
            'fecha_caducidad'=>'Fecha Caducidad'            
        ];
    }
}
