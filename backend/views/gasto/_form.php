<?php

use yii\helpers\Html;
use app\models\Servicio;
use app\components\THtml;
use app\models\Cooperativa;
use app\models\Trabajador;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ServicioCampo;

/* @var $this yii\web\View */
/* @var $model app\models\Gasto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gasto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $options = ArrayHelper::map(Servicio::find()->asArray()->all(), 'id', "nombre");
    echo $form->field($model, 'servicio_id')->dropDownList($options, ['prompt' => 'Seleccione servicio...']);
    ?>

    <?= $form->field($model, 'concepto')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'fecha')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= THtml::autocomplete($model,'trabajador_id', ['trabajador/lookup'], 'trabajador');?>

    <?= $form->field($model, 'total')->textInput(['type' => 'number']) ?>
    
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>