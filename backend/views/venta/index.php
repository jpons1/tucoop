<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VentaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ventas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="venta-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php 
    if (isset(Yii::$app->user->identity) && Yii::$app->user->identity->rol == "A") {
    ?>
    <p>
        <?= Html::a(Yii::t('app', 'Crear Venta'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombreProducto',
            'comprador',
            'temporada_id',
            'cantidad',
            'precio',
            'fecha',
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]) ?>
</div>