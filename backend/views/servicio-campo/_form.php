<?php

use app\models\Campo;
use yii\helpers\Html;
use yii\jui\DatePicker;
use app\models\Servicio;
use app\components\THtml;
use app\models\Cooperativa;
use app\models\Temporada;
use app\models\Trabajador;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioCampo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servicio-campo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $options = ArrayHelper::map(Servicio::find()->asArray()->all(), 'id', 'nombre');
    echo $form->field($model, 'servicio_id')->dropDownList($options, ['prompt' => 'Seleccione servicio...']);
    ?>

    <?= THtml::autocomplete($model, 'campo_id', ['campo/lookup'], 'campo'); ?>

    <?php $options = ArrayHelper::map(Temporada::find()->asArray()->all(), 'id', 'nombre');
    echo $form->field($model, 'temporada_id')->dropDownList($options, ['prompt' => 'Seleccione temporada...']);
    ?>

    <?= $form->field($model, 'gasto')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'fecha_contrato')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'fecha_vencimiento')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'beneficio')->textInput(['type' => 'number']) ?>

    <?= THtml::autocomplete($model, 'trabajador_id', ['trabajador/lookup'], 'trabajador'); ?>
    
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>