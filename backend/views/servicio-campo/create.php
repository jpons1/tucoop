<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioCampo */

$this->title = Yii::t('app', 'Crear trabajo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Servicio Campos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-campo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
