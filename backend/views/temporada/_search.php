<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TemporadaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="temporada-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class='row'>
        <div class='col-md-4'>
            <?= $form->field($model, 'nombre') ?>
        </div>
        <div class='col-md-4'>
            <?= $form->field($model, 'fecha_inicio') ?>
        </div>
        <div class='col-md-4'>
            <?= $form->field($model, 'fecha_fin') ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>