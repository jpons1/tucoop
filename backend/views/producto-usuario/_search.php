<?php

use yii\helpers\Html;
use app\models\Usuario;
use app\models\Producto;
use app\components\THtml;
use app\models\Temporada;
use app\models\Trabajador;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\ProductoUsuarioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="producto-usuario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class='row'>
        <div class='col-md-3'>
            <?= THtml::autocomplete($model,'usuario_id', ['usuario/lookup'], 'usuario_id');?>
        </div>
        <div class='col-md-3'>
            <?php $options = ArrayHelper::map(Producto::find()->asArray()->all(), 'id', 'nombre');
            echo $form->field($model, 'producto_id')->dropDownList($options, ['prompt' => 'Seleccione producto...']);
            ?>
        </div>
        <div class='col-md-3'>
            <?php $options = ArrayHelper::map(Temporada::find()->asArray()->all(), 'id', 'nombre');
            echo $form->field($model, 'temporada_id')->dropDownList($options, ['prompt' => 'Seleccione temporada...']);
            ?>
        </div>
        <div class='col-md-3'>
            <?= $form->field($model, 'fecha') ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>