<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\cooperativa */
/* @var $form ActiveForm */
/*<?= $form->field($model, 'fecha_caducidad')
        ->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd'])
        ?> */
        $var=[0=>'No ampliar',1=>'1 mes'];
        for ($i=2; $i <=12 ; $i++) { 
            $var[]=$i.' meses';
        }
?>
<div class="cooperativa-_form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'cif')->textInput(['type' => 'number']) ?>
        <?= $form->field($model, 'fecha_caducidad')
        ->dropDownList($var,['promt'=>'Seleccione Uno'])
        ?>
        <?= $form->field($model, 'limite_socios')->textInput(['type' => 'number'])  ?>
       
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- cooperativa-_form -->
