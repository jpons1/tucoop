<?php

//menu de la iqz
use yii\helpers\Url;
use yii\helpers\Html;
use adminlte\widgets\Menu;


?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->

        <div class="user-panel">
            <?php
            if (Yii::$app->user->identity) { //si esta logueado

            ?>
                <div class="pull-left image">
                    <?= Html::img('https://localhost/tucoop/backend/imagenes/perfil/' . Yii::$app->user->identity->imagen_src, ['class' => 'img-circle', 'alt' => 'User Image']) ?>
                </div>
               
                <div class="pull-left info">
                    <p><?= Yii::$app->user->identity->nombre ?> </p>
                    <a href="#"><i class="fa fa-circle text-success"></i>En linea</a>
                </div>
            <?php
            }
            ?>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Buscar...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?=
        Menu::widget( //routing
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Cooperativa', 'icon' => '',
                        'url' => ['/cooperativa'], 'active' => $this->context->route == 'site/index'
                    ],
                    [
                        'label' => 'Ventas', 'icon' => 'fa fa-balance-scale',
                        'url' => ['/'], 'active' => $this->context->route == 'site/index' //url es la url que quieres que salga de url y active carga el index
                    ],
                    [
                        'label' => 'Gastos', 'icon' => 'fa fa-balance-scale',
                        'url' => ['/gasto'], 'active' => $this->context->route == 'site/index'
                    ],
                    [
                        'label' => 'Productos', 'icon' => 'fa fa-leaf',
                        'url' => ['/producto'], 'active' => $this->context->route == 'site/index'
                    ],
                    [
                        'label' => 'Temporadas', 'icon' => 'fa fa-calendar',
                        'url' => ['/temporada'], 'active' => $this->context->route == 'site/index'
                    ],
                    [
                        'label' => 'Servicios', 'icon' => 'fa fa-wrench',
                        'url' => ['/servicio'], 'active' => $this->context->route == 'site/index'
                    ],
                    [
                        'label' => 'Campos', 'icon' => 'fa fa-tree',
                        'url' => ['/campo'], 'active' => $this->context->route == 'site/index'
                    ],
                    [
                        'label' => 'Trabajadores', 'icon' => 'fa fa-male',
                        'url' => ['/trabajador'], 'active' => $this->context->route == 'site/index'
                    ],
                    [
                        'label' => 'Noticias', 'icon' => 'fa fa-paragraph',
                        'url' => ['/noticia'], 'active' => $this->context->route == 'site/index'
                    ],
                    [
                        'label' => 'Entregas', 'icon' => 'fa fa-truck',
                        'url' => ['/producto-usuario'], 'active' => $this->context->route == 'site/index'
                    ],
                    [
                        'label' => 'Trabajos', 'icon' => 'fa fa-clipboard',
                        'url' => ['/servicio-campo'], 'active' => $this->context->route == 'site/index'
                    ],
                    [
                        'label' => 'Usuarios', 'icon' => 'fa fa-users',
                        'url' => ['/usuario'], 'active' => $this->context->route == 'user/index',
                    ]
                ],
            ]
        )
        ?>

    </section>
    <!-- /.sidebar -->
</aside>