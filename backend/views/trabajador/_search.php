<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrabajadorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trabajador-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class='row'>
        <div class='col-md-3'>
            <?= $form->field($model, 'nombre') ?>
        </div>
        <div class='col-md-3'>
            <?= $form->field($model, 'apellidos') ?>
        </div>
        <div class='col-md-3'>
            <?php echo $form->field($model, 'ciudad') ?>
        </div>
        <div class='col-md-3'>
            <?php echo $form->field($model, 'codigo_postal') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>