<?php

namespace app\controllers;

use Yii;
use app\models\Noticia;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\NoticiaSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
/**
 * NoticiaController implements the CRUD actions for Noticia model.
 */
class NoticiaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'delete', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'delete', 'update'],
                        'matchCallback' => function ($rule, $action) {
                            return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol == "A";
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Noticia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NoticiaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $coop=$_GET['cooperativa']??"";
        if(!$coop==""){
            $dataProvider=
                Yii::$app->db->
                    createCommand("SELECT * FROM noticia WHERE cooperativa_id=$coop")
                    ->queryAll();
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Noticia model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Noticia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Noticia(['scenario'=>'create']);

        if($model->load(Yii::$app->request->post())) {

            $model->imagen_src = UploadedFile::getInstance($model, 'imagen_src');  
            $cod = uniqid();
            $model->imagen_src->saveAs('../web/imagenes/noticias/' . $cod . '.' . $model->imagen_src->extension);
            
            $model->imagen_src = $cod . '.' . $model->imagen_src->extension;

            if ($model->save()) {            
                return $this->redirect(['view', 'id' => $model->id]);
            }    
        }

        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Servicio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //GUARDO LA IMG VIEJA
        $img = $model->imagen_src;

        if($model->load(Yii::$app->request->post())) {

            //SI NO TIENE IMAGEN, LA METO
            if ($model->imagen_src =='') {                          
                $model->imagen_src=$img;  
            //SI TIENE UNA IMAGEN, BORRO LA VIEJA Y METO LA NUEVA
            } else {
                //BORRO LA IMG VIEJA
                unlink('../web/imagenes/noticias/' . $img);
                
                $model->imagen_src = UploadedFile::getInstance($model, 'imagen_src');  
                $cod = uniqid();
                $model->imagen_src->saveAs('../web/imagenes/noticias/' . $cod . '.' . $model->imagen_src->extension);
               
                $model->imagen_src = $cod . '.' . $model->imagen_src->extension;

            } 

            if ($model->save()) {                
                return $this->redirect(['view', 'id' => $model->id]);
            } 
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Noticia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //GUARDO LA FOTO EN UNA VARIABLE
        $borrar = $this->findModel($id);
        $img = $borrar->imagen_src;
        //SI NO HAY FOTO, BORRO SOLO DATOS
        if ($img == '') {
            $borrar->delete();
        //SINO, BORRO DATOS Y FOTO    
        } else {
            unlink('../web/imagenes/noticias/' . $img);
            $borrar->delete();
        }    
        return $this->redirect(['index']);
    }

    /**
     * Finds the Noticia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Noticia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Noticia::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
