<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "temporada".
 *
 * @property int $id
 * @property string $nombre
 * @property string $fecha_inicio
 * @property string $fecha_fin
 *
 * @property ProductoUsuario[] $productoUsuarios
 * @property ServicioCampo[] $servicioCampos
 * @property Venta[] $ventas
 */
class Temporada extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temporada';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'fecha_inicio', 'fecha_fin'], 'required'],
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['nombre'], 'string', 'max' => 30],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'fecha_inicio' => Yii::t('app', 'Fecha Inicio'),
            'fecha_fin' => Yii::t('app', 'Fecha Fin'),
        ];
    }

    /**
     * Gets query for [[ProductoUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductoUsuarios()
    {
        return $this->hasMany(ProductoUsuario::className(), ['temporada_id' => 'id']);
    }

    /**
     * Gets query for [[ServicioCampos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicioCampos()
    {
        return $this->hasMany(ServicioCampo::className(), ['temporada_id' => 'id']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Venta::className(), ['temporada_id' => 'id']);
    }

    function beforeSave($insert) {
        if($this->isNewRecord){
            $this->id=count(Temporada::find()->asArray()->all())+1;
        }  
        return parent::beforeSave($insert);
      }
}
