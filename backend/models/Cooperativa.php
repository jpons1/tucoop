<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cooperativa".
 *
 * @property int $id
 * @property string $nombre
 * @property string $direccion
 * @property int $cif
 *
 * @property Campo[] $campos
 * @property Gasto[] $gastos
 * @property Noticia[] $noticias
 * @property Producto[] $productos
 * @property ProductoUsuario[] $productoUsuarios
 * @property Servicio[] $servicios
 * @property ServicioCampo[] $servicioCampos
 * @property Temporada[] $temporadas
 * @property Trabajador[] $trabajadors
 * @property Usuario[] $usuarios
 * @property Venta[] $ventas
 */
class Cooperativa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cooperativa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'direccion', 'cif'], 'required'],
            [['fecha_caducidad'],'safe'],
            [['id', 'cif','limite_socios','precio'], 'integer'],
            [['nombre'], 'string', 'max' => 32],
            [['direccion'], 'string', 'max' => 128],
        ];
    }

     
    function beforeSave($insert) {
        $cadActual=0;
        $tSCadActual=0;
        if($this->isNewRecord){
            $this->id=count(Cooperativa::find()->asArray()->all())+1;
        }  else{
            $cadActual=(new \yii\db\Query())
            ->select('fecha_caducidad')
            ->from('cooperativa')
            ->where('id='.$this->id)
            ->all()[0]["fecha_caducidad"];
            $tSCadActual=strtotime($cadActual);
        }
        $timestamp=getdate()[0];
       
        
        if($tSCadActual>=$timestamp){
            $timestamp=strtotime(+$this->fecha_caducidad.'month', $tSCadActual);
        }else{
            $timestamp=strtotime(+$this->fecha_caducidad.'month', getdate()[0]);
        }
        //Precio por usuario por mes
        $precioUsuarioMes=15;
        $l_s=$this->limite_socios;
        if($l_s>=100){
            $precioUsuarioMes=2;
        }else if($l_s>=50){
            $precioUsuarioMes=6;
        }else if($l_s>=10){
            $precioUsuarioMes=10;
        }
        $f=date_create();
        date_timestamp_set($f, $timestamp);
        echo "<pre>";
        //var_dump($this->fecha_caducidad);
        $this->precio= $precioUsuarioMes*$l_s*intval($this->fecha_caducidad);
        $this->fecha_caducidad=date_format($f,'Y-m-d');
       
       
        
        //echo "<pre>";
        /*var_dump(getdate()[0]);
        var_dump($this->fecha_caducidad);
        var_dump($insert);
        */
        var_dump($this->precio);
        echo "<pre>";
        var_dump($precioUsuarioMes*$l_s*intval($this->fecha_caducidad));
        //die;
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'cif' => 'Cif',
            'limite_socios' => 'Limite Socios',
            'precio' => 'Precio',
            'fecha_caducidad'=>'Fecha Caducidad',
        ];
    }

    /**
     * Gets query for [[Campos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCampos()
    {
        return $this->hasMany(Campo::className(), ['cooperativa_id' => 'id']);
    }

    /**
     * Gets query for [[Gastos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGastos()
    {
        return $this->hasMany(Gasto::className(), ['cooperativa_id' => 'id']);
    }

    /**
     * Gets query for [[Noticias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticias()
    {
        return $this->hasMany(Noticia::className(), ['cooperativa_id' => 'id']);
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Producto::className(), ['cooperativa_id' => 'id']);
    }

    /**
     * Gets query for [[ProductoUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductoUsuarios()
    {
        return $this->hasMany(ProductoUsuario::className(), ['cooperativa_id' => 'id']);
    }

    /**
     * Gets query for [[Servicios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(Servicio::className(), ['cooperativa_id' => 'id']);
    }

    /**
     * Gets query for [[ServicioCampos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicioCampos()
    {
        return $this->hasMany(ServicioCampo::className(), ['cooperativa_id' => 'id']);
    }

    /**
     * Gets query for [[Temporadas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTemporadas()
    {
        return $this->hasMany(Temporada::className(), ['cooperativa_id' => 'id']);
    }

    /**
     * Gets query for [[Trabajadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajadors()
    {
        return $this->hasMany(Trabajador::className(), ['cooperativa_id' => 'id']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['cooperativa_id' => 'id']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Venta::className(), ['cooperativa_id' => 'id']);
    }

   
}
