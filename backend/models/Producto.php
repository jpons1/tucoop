<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property int $id
 * @property string $nombre
 *
 * @property ProductoUsuario[] $productoUsuarios
 * @property Venta[] $ventas
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 30],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * Gets query for [[ProductoUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductoUsuarios()
    {
        return $this->hasMany(ProductoUsuario::className(), ['producto_id' => 'id']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Venta::className(), ['producto_id' => 'id']);
    }

    function beforeSave($insert) {
        if($this->isNewRecord){
            $this->id=count(Producto::find()->asArray()->all())+1;
        }  
        return parent::beforeSave($insert);
      }
}
