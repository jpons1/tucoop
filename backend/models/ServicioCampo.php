<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicio_campo".
 *
 * @property int $id
 * @property int $servicio_id
 * @property int $campo_id
 * @property int $temporada_id
 * @property int $gasto
 * @property string $fecha_contrato
 * @property string $fecha_vencimiento
 * @property int $beneficio
 * @property int $trabajador_id
 *
 * @property Gasto[] $gastos
 * @property Trabajador $trabajador
 * @property Campo $campo
 * @property Servicio $servicio
 * @property Temporada $temporada
 */
class ServicioCampo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servicio_campo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['servicio_id', 'campo_id', 'temporada_id', 'gasto', 'fecha_contrato', 'fecha_vencimiento', 'beneficio', 'trabajador_id'], 'required'],
            [['servicio_id', 'campo_id', 'temporada_id', 'gasto', 'beneficio', 'trabajador_id'], 'integer'],
            [['fecha_contrato', 'fecha_vencimiento'], 'safe'],
            [['trabajador_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trabajador::className(), 'targetAttribute' => ['trabajador_id' => 'id']],
            [['campo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Campo::className(), 'targetAttribute' => ['campo_id' => 'id']],
            [['servicio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['servicio_id' => 'id']],
            [['temporada_id'], 'exist', 'skipOnError' => true, 'targetClass' => Temporada::className(), 'targetAttribute' => ['temporada_id' => 'id']],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'servicio_id' => Yii::t('app', 'Servicio ID'),
            'campo_id' => Yii::t('app', 'Campo ID'),
            'temporada_id' => Yii::t('app', 'Temporada ID'),
            'gasto' => Yii::t('app', 'Gasto'),
            'fecha_contrato' => Yii::t('app', 'Fecha Contrato'),
            'fecha_vencimiento' => Yii::t('app', 'Fecha Vencimiento'),
            'beneficio' => Yii::t('app', 'Beneficio'),
            'trabajador_id' => Yii::t('app', 'Trabajador ID'),
        ];
    }

    /**
     * Gets query for [[Gastos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGastos()
    {
        return $this->hasMany(Gasto::className(), ['servicio_id' => 'id']);
    }

    /**
     * Gets query for [[Trabajador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajador()
    {
        return $this->hasOne(Trabajador::className(), ['id' => 'trabajador_id']);
    }

    /**
     * Gets query for [[Campo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCampo()
    {
        return $this->hasOne(Campo::className(), ['id' => 'campo_id']);
    }

    /**
     * Gets query for [[Servicio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(Servicio::className(), ['id' => 'servicio_id']);
    }

    /**
     * Gets query for [[Temporada]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTemporada()
    {
        return $this->hasOne(Temporada::className(), ['id' => 'temporada_id']);
    }

    public function getNombreServicio()
    {
        return $this->servicio->nombre;
    }

    public function getNombreTrabajador()
    {
        return $this->trabajador->apellidos . ", " . $this->trabajador->nombre;
    }

    public function getDireccion()
    {
        return $this->campo->ciudad. ", " . $this->campo->direccion;
    }
}
