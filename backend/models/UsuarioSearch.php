<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Usuario;

/**
 * UsuarioSearch represents the model behind the search form of `app\models\Usuario`.
 */
class UsuarioSearch extends Usuario
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'codigo_postal', 'telefono', 'cuota','cooperativa_id'], 'integer'],
            [['nombre', 'apellidos', 'mail', 'direccion', 'ciudad', 'dni', 'rol', 'contrasenya', 'IBAN', 'imagen_src'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    { 
        $coop = CooperativaHelper::cooperativa_en_sesion()['id']??0;
        $query = Usuario::find()->where("cooperativa_id=$coop");
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>['pageSize'=>5]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'codigo_postal' => $this->codigo_postal,
            'telefono' => $this->telefono,
            'cuota' => $this->cuota,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'mail', $this->mail])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'ciudad', $this->ciudad])
            ->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'rol', $this->rol])
            ->andFilterWhere(['like', 'contrasenya', $this->contrasenya])
            ->andFilterWhere(['like', 'IBAN', $this->IBAN])
            ->andFilterWhere(['like', 'imagen_src', $this->imagen_src]);

        return $dataProvider;
    }
}
