<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Venta;

//require_once (dirname(__FILE__).'/helpers/CooperativaHelper.php');


class VentaSearch extends Venta
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'producto_id', 'precio', 'cantidad', 'temporada_id', 'cooperativa_id'], 'integer'],
            [['fecha', 'comprador'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $coop = CooperativaHelper::cooperativa_en_sesion()['id']??0;
        $query = Venta::find()->where("cooperativa_id=$coop");
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 5]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'producto_id' => $this->producto_id,
            'fecha' => $this->fecha,
            'precio' => $this->precio,
            'cantidad' => $this->cantidad,
            'temporada_id' => $this->temporada_id,
        ]);

        $query->andFilterWhere(['like', 'comprador', $this->comprador]);

        return $dataProvider;
    }
}
