<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto_usuario".
 *
 * @property int $id
 * @property int $usuario_id
 * @property int $producto_id
 * @property int $temporada_id
 * @property int $cantidad
 * @property string $fecha
 * @property int|null $gasto
 * @property int|null $beneficio
 *
 * @property Producto $producto
 * @property Temporada $temporada
 * @property Usuario $usuario
 */
class ProductoUsuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto_usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id', 'producto_id', 'temporada_id', 'cantidad', 'fecha'], 'required'],
            [['usuario_id', 'producto_id', 'temporada_id', 'cantidad', 'gasto', 'beneficio'], 'integer'],
            [['fecha'], 'safe'],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id' => 'id']],
            [['temporada_id'], 'exist', 'skipOnError' => true, 'targetClass' => Temporada::className(), 'targetAttribute' => ['temporada_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'usuario_id' => Yii::t('app', 'Usuario ID'),
            'producto_id' => Yii::t('app', 'Producto ID'),
            'temporada_id' => Yii::t('app', 'Temporada ID'),
            'cantidad' => Yii::t('app', 'Cantidad'),
            'fecha' => Yii::t('app', 'Fecha'),
            'gasto' => Yii::t('app', 'Gasto'),
            'beneficio' => Yii::t('app', 'Beneficio'),
        ];
    }

    /**
     * Gets query for [[Producto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id']);
    }

    /**
     * Gets query for [[Temporada]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTemporada()
    {
        return $this->hasOne(Temporada::className(), ['id' => 'temporada_id']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    public function getNombreUsuario(){
        return $this->usuario->apellidos . ", " . $this->usuario->nombre;
    }
    
    public function getNombreProducto(){
        return $this->producto->nombre;
    }
}
