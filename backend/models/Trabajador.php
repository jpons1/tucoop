<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trabajador".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $dni
 * @property string $mail
 * @property string $telefono
 * @property string $direccion
 * @property string $ciudad
 * @property int $codigo_postal
 * @property string $IBAN
 *
 * @property Gasto[] $gastos
 * @property ServicioCampo[] $servicioCampos
 */
class Trabajador extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trabajador';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'dni', 'mail', 'telefono', 'direccion', 'ciudad', 'codigo_postal', 'IBAN','cooperativa_id'], 'required'],
            [['codigo_postal'], 'integer'],
            [['nombre', 'mail', 'ciudad'], 'string', 'max' => 30],
            [['apellidos', 'direccion'], 'string', 'max' => 60],
            [['dni', 'telefono'], 'string', 'max' => 9],
            [['IBAN'], 'string', 'max' => 26],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'apellidos' => Yii::t('app', 'Apellidos'),
            'dni' => Yii::t('app', 'Dni'),
            'mail' => Yii::t('app', 'Mail'),
            'telefono' => Yii::t('app', 'Telefono'),
            'direccion' => Yii::t('app', 'Direccion'),
            'ciudad' => Yii::t('app', 'Ciudad'),
            'codigo_postal' => Yii::t('app', 'Codigo Postal'),
            'IBAN' => Yii::t('app', 'Iban'),
        ];
    }

    /**
     * Gets query for [[Gastos]].
     *
     * @return \yii\db\ActiveQuery
     */
    
    public function __toString()
    {
        return $this->apellidos . ", " . $this->nombre;
    }
     public function getGastos()
    {
        return $this->hasMany(Gasto::className(), ['trabajador_id' => 'id']);
    }

    /**
     * Gets query for [[ServicioCampos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicioCampos()
    {
        return $this->hasMany(ServicioCampo::className(), ['trabajador_id' => 'id']);
    }

    function beforeSave($insert) {
        if($this->isNewRecord){
            $this->id=count(Trabajador::find()->asArray()->all())+1;
        }  
        return parent::beforeSave($insert);
    }


}
