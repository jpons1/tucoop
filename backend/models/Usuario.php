<?php
//aqui estan los metodos que yii necesita para hacer login
namespace app\models;

use Yii;
use app\models\Campo;
use yii\web\IdentityInterface;// es la clase que se utliza para loguearse
use app\models\ProductoUsuario;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $mail
 * @property string $direccion
 * @property string $ciudad
 * @property int $codigo_postal
 * @property int $telefono
 * @property string $dni
 * @property string $rol
 * @property string $contrasenya
 * @property int $cuota
 * @property string $IBAN
 * @property string $imagen_src
 *
 * @property Campo[] $campos
 * @property ProductoUsuario[] $productoUsuarios
 */

class Usuario extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'mail', 'direccion', 'ciudad', 'codigo_postal', 'telefono', 'dni', 'rol', 'contrasenya', 'cuota', 'IBAN'], 'required'],
            [['imagen_src'], 'required', 'on' => 'create'],
            [['codigo_postal', 'telefono', 'cuota'], 'integer'],
            [['nombre', 'apellidos', 'mail', 'ciudad'], 'string', 'max' => 30],
            [['direccion', 'imagen_src'], 'string', 'max' => 60],
            [['dni'], 'string', 'max' => 9],
            [['rol'], 'string', 'max' => 1],
            [['contrasenya'], 'string', 'max' => 32],
            [['IBAN'], 'string', 'max' => 26],
            [['fecha_caducidad'], 'safe'],
            [['caducidad_token'], 'string', 'max' => 64],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'apellidos' => Yii::t('app', 'Apellidos'),
            'mail' => Yii::t('app', 'Mail'),
            'direccion' => Yii::t('app', 'Direccion'),
            'ciudad' => Yii::t('app', 'Ciudad'),
            'codigo_postal' => Yii::t('app', 'Codigo Postal'),
            'telefono' => Yii::t('app', 'Telefono'),
            'dni' => Yii::t('app', 'Dni'),
            'rol' => Yii::t('app', 'Rol'),
            'contrasenya' => Yii::t('app', 'Contrasenya'),
            'cuota' => Yii::t('app', 'Cuota'),
            'IBAN' => Yii::t('app', 'Iban'),
            'imagen_src' => Yii::t('app', 'Imagen Src'),
            'token' => Yii::t('app', 'Token'),
            'caducidad_token' => Yii::t('app', 'Caducidad'),
        ];
    }

    public function __toString()
    {
        return $this->apellidos . ", " . $this->nombre;
    }

    /**
     * Gets query for [[Campos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCampos()
    {
        return $this->hasMany(Campo::className(), ['usuario_id' => 'id']);
    }

    /**
     * Gets query for [[ProductoUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductoUsuarios()
    {
        return $this->hasMany(ProductoUsuario::className(), ['usuario_id' => 'id']);
    }

    public static function findByMail($username)
    {
        return static::findOne(['mail' => $username]);
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
    }

    public function validateAuthKey($authKey)
    {
    }
    public static function findIdentityByAccessToken($token, $type = null)
    {
    }

    // Comprueba que el password que se le pasa es correcto
    public function validatePassword($password)
    {
        return $this->contrasenya === md5($password); // Si se utiliza otra función de encriptación distinta a md5, habrá que cambiar esta línea
    }

    function hasRole($role)
    {
        return $this->rol == $role;
    }

    function beforeSave($insert)//se ejecuta justo antes de hacer el save, de insertar algo en la bd
    {
        if ($this->isNewRecord) {
            $this->id = count(Usuario::find()->asArray()->all()) + 1;
        }

        return parent::beforeSave($insert);
    }

    public function getRolUsuario(){//sirve para mostrar "administrador" en vez de "A", etc
        if ($this->rol == "A") {
            return "Administrador";
        }else if($this->rol == "U"){
            return "Usuario";
        }
    }
}
