-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-06-2021 a las 09:48:54
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tucoop`
--
CREATE DATABASE IF NOT EXISTS `tucoop` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci;
USE `tucoop`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campo`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `campo`;
CREATE TABLE `campo` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `tamanyo` int(11) NOT NULL,
  `direccion` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `ciudad` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_postal` int(11) NOT NULL,
  `cooperativa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `campo`
--

INSERT INTO `campo` (`id`, `usuario_id`, `tamanyo`, `direccion`, `ciudad`, `codigo_postal`, `cooperativa_id`) VALUES
(1, 2, 1000, 'direccion', 'Alaquas', 46970, 0),
(2, 2, 520, 'direccion', 'Xirivella', 46950, 0),
(3, 5, 750, 'direccion', 'Xirivella', 46950, 0),
(4, 4, 300, 'direccion', 'Torrente', 46901, 0),
(5, 3, 1200, 'direccion', 'Valencia', 46000, 0),
(6, 5, 140, 'direccion', 'Xirivella', 46950, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cooperativa`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `cooperativa`;
CREATE TABLE `cooperativa` (
  `id` int(11) NOT NULL,
  `nombre` varchar(32) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `direccion` varchar(128) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cif` int(11) NOT NULL,
  `limite_socios` int(11) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL,
  `fecha_caducidad` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cooperativa`
--

INSERT INTO `cooperativa` (`id`, `nombre`, `direccion`, `cif`, `limite_socios`, `precio`, `fecha_caducidad`) VALUES
(0, 'Tucoop', '12', 22211, NULL, NULL, NULL),
(1, 'NoCoop', 'f', 99999, 9, 180, '0000-00-00'),
(2, 'Coperativa Santa Ana', 'fdbhdhi', 2147483647, 90, 1800, '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `gasto`;
CREATE TABLE `gasto` (
  `id` int(11) NOT NULL,
  `servicio_id` int(11) NOT NULL,
  `concepto` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` date NOT NULL,
  `trabajador_id` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `cooperativa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `gasto`
--

INSERT INTO `gasto` (`id`, `servicio_id`, `concepto`, `descripcion`, `fecha`, `trabajador_id`, `total`, `cooperativa_id`) VALUES
(1, 1, 'Tasa trabajador', 'GHJgjshadvduyfd', '2021-01-20', 1, 300, 0),
(2, 2, 'Tasa Nadine', 'safdddssssssssssssssssssssdfsafdafs', '2021-02-14', 2, 500, 0),
(3, 3, 'Tasa Pablo', 'asdfffffffffffffffffffsadfasdfaadf', '2021-02-16', 3, 200, 0),
(4, 1, 'Un conceptaco', 'El buen concepto', '2021-05-06', 3, 15, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `noticia`;
CREATE TABLE `noticia` (
  `id` int(11) NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `titular` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `cuerpo` varchar(1000) COLLATE utf8_spanish2_ci NOT NULL,
  `imagen_src` varchar(28) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cooperativa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticia`
--

INSERT INTO `noticia` (`id`, `fecha_hora`, `titular`, `cuerpo`, `imagen_src`, `cooperativa_id`) VALUES
(1, '2021-01-01 00:00:00', '¡FELIZ AÑO NUEVO!', 'Desde TuCoop os deseamos un feliz año 2021', '602ce46ada43d.jpg', 0),
(2, '2020-12-19 00:00:00', 'Lore ipsum', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus ', '602ce3d29ca2a.jpg', 0),
(3, '2021-01-10 00:00:00', 'Lore ipsum2', '2 - Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus ', '602cc5dfa08ef.jpg', 0),
(4, '2021-01-06 00:00:00', 'lore ipsum 3', '3 - Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus ', '602cc66dc1978.jpg', 0),
(5, '2020-12-09 00:00:00', 'Lore ipsum 4', '4 - Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus ', '602cc6a959a28.jpg', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `cooperativa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `nombre`, `cooperativa_id`) VALUES
(1, 'Naranja', 0),
(2, 'Pera', 0),
(3, 'Platano', 0),
(4, 'Pomelo', 0),
(5, 'Manzana', 0),
(6, 'Arroz', 0),
(7, 'Prod milagroso', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_usuario`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `producto_usuario`;
CREATE TABLE `producto_usuario` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `temporada_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `gasto` int(11) DEFAULT NULL,
  `beneficio` int(11) DEFAULT NULL,
  `cooperativa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `producto_usuario`
--

INSERT INTO `producto_usuario` (`id`, `usuario_id`, `producto_id`, `temporada_id`, `cantidad`, `fecha`, `gasto`, `beneficio`, `cooperativa_id`) VALUES
(1, 2, 1, 1, 300, '2021-01-06', 100, 300, 0),
(2, 2, 2, 1, 500, '2021-01-21', 150, 400, 0),
(3, 2, 3, 2, 700, '2021-02-02', 300, 600, 0),
(4, 2, 1, 2, 345, '2021-02-18', 123, 380, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `servicio`;
CREATE TABLE `servicio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `precio_metro` int(11) NOT NULL,
  `imagen_src` varchar(28) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cooperativa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id`, `nombre`, `precio_metro`, `imagen_src`, `cooperativa_id`) VALUES
(1, 'Fumigar', 5, '602ccf7559f0c.jpg', 1),
(2, 'Recoger', 10, '602ccf83e9717.jpg', 1),
(3, 'Desinfectar', 15, '602ccf92109b8.jpg', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_campo`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `servicio_campo`;
CREATE TABLE `servicio_campo` (
  `id` int(11) NOT NULL,
  `servicio_id` int(11) NOT NULL,
  `campo_id` int(11) NOT NULL,
  `temporada_id` int(11) NOT NULL,
  `gasto` int(11) NOT NULL,
  `fecha_contrato` date NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `beneficio` int(11) NOT NULL,
  `trabajador_id` int(11) NOT NULL,
  `cooperativa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `servicio_campo`
--

INSERT INTO `servicio_campo` (`id`, `servicio_id`, `campo_id`, `temporada_id`, `gasto`, `fecha_contrato`, `fecha_vencimiento`, `beneficio`, `trabajador_id`, `cooperativa_id`) VALUES
(1, 1, 1, 1, 500, '2021-01-05', '2021-01-14', 1000, 1, 0),
(2, 2, 1, 2, 200, '2021-02-03', '2021-02-10', 500, 2, 1),
(3, 3, 1, 2, 700, '2021-02-09', '2021-02-12', 1000, 3, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temporada`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `temporada`;
CREATE TABLE `temporada` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `cooperativa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `temporada`
--

INSERT INTO `temporada` (`id`, `nombre`, `fecha_inicio`, `fecha_fin`, `cooperativa_id`) VALUES
(1, '1', '2021-01-01', '2021-01-31', 1),
(2, '2', '2021-02-01', '2021-02-15', 0),
(3, '3', '2021-02-16', '2021-02-28', NULL),
(4, '4', '2021-03-01', '2021-04-30', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `trabajador`;
CREATE TABLE `trabajador` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidos` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `dni` varchar(9) COLLATE utf8_spanish2_ci NOT NULL,
  `mail` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(9) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `ciudad` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_postal` int(11) NOT NULL,
  `IBAN` varchar(26) COLLATE utf8_spanish2_ci NOT NULL,
  `cooperativa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `trabajador`
--

INSERT INTO `trabajador` (`id`, `nombre`, `apellidos`, `dni`, `mail`, `telefono`, `direccion`, `ciudad`, `codigo_postal`, `IBAN`, `cooperativa_id`) VALUES
(1, 'Javier', 'Gómez Martínez', '12345678A', 'javier@gmail.com', '123456789', 'direccion', 'ciudad', 12345, '111111111111111', 1),
(2, 'Nadine', 'Martínez Sánchez', '12345678A', 'nadine@gmail.com', '123456789', 'direccion', 'ciudad', 12345, '111111111111111', NULL),
(3, 'Pablo', 'Perico Chus', '12345678A', 'pablo@gmail.com', '123456789', 'direccion', 'ciudad', 12345, '111111111111111', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidos` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `mail` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `ciudad` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_postal` int(11) NOT NULL,
  `telefono` int(11) NOT NULL,
  `dni` varchar(9) COLLATE utf8_spanish2_ci NOT NULL,
  `rol` char(1) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasenya` varchar(32) COLLATE utf8_spanish2_ci NOT NULL,
  `cuota` int(11) NOT NULL,
  `IBAN` varchar(26) COLLATE utf8_spanish2_ci NOT NULL,
  `imagen_src` varchar(28) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `token` varchar(64) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `caducidad_token` date DEFAULT NULL,
  `cooperativa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `apellidos`, `mail`, `direccion`, `ciudad`, `codigo_postal`, `telefono`, `dni`, `rol`, `contrasenya`, `cuota`, `IBAN`, `imagen_src`, `token`, `caducidad_token`, `cooperativa_id`) VALUES
(1, 'admin', 'admin', 'admin@admin.com', 'c/admin', 'admin', 12345, 123456789, 'admin', 'A', '21232f297a57a5a743894a0e4a801fc3', 0, 'admin', '602cca2027c41.jpg', '613f2430014105d36d48f760323187bf6f33b4ab2214bfd72abcf1150fbffef3', NULL, 0),
(2, 'Alejandro', 'Sánchez Anguix', 'asanchez39@gmail.com', 'c/ Mayor', 'Alaquas', 46970, 123456789, '12345678A', 'U', '0023163728502400bf638d4318616eae', 300, '1234567891213', '602cc70b51a78.jpg', '0973cacf4ca708911b041f8d3e833b62388d74b6ca54809116921464ca504685', NULL, 1),
(3, 'Jorge', 'Tarin Ballester', 'jtarin2@iesfuentesanluis.org', 'c/Imaginaria', 'Valencia', 46001, 123456789, '12345678A', 'U', 'd67326a22642a324aa1b0745f2f17abb', 350, '1234567891213', '602cc71e8473e.jpg', NULL, NULL, 1),
(4, 'Jonathan', 'Perez Toledo', 'jperez3@iesfuentesanluis.org', 'c/Ilusión', 'Torrente', 46900, 123456789, '12345678A', 'U', '78842815248300fa6ae79f7776a5080a', 200, '1234567891213', '602cc766e5629.jpg', NULL, NULL, 0),
(5, 'Jaume', 'Pons Orti', 'jpons@iesfuentesanluis.org', 'c/Alegria', 'Xirivella', 46950, 123456789, '12345678A', 'U', '376cd34d7d267a83caa95ca10dbecd98', 500, '12345845468', '602cc9bc74454.jpg', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--
-- Creación: 16-06-2021 a las 08:05:41
--

DROP TABLE IF EXISTS `venta`;
CREATE TABLE `venta` (
  `id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `precio` int(11) NOT NULL,
  `comprador` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `temporada_id` int(11) NOT NULL,
  `cooperativa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `producto_id`, `fecha`, `precio`, `comprador`, `cantidad`, `temporada_id`, `cooperativa_id`) VALUES
(1, 1, '2021-01-04', 1000, 'Paco Merte', 750, 1, 1),
(2, 2, '2021-01-07', 301, 'Penelope Ruiz', 200, 1, 0),
(3, 1, '2021-02-10', 1200, 'Mia Loco', 1050, 2, 0),
(4, 3, '2021-05-19', 4, 'f', 2, 2, 0),
(5, 5, '2021-05-01', 15, 'Pepito Grillo', 2, 1, 0),
(6, 5, '2021-05-19', 500, 'M', 9656, 1, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `campo`
--
ALTER TABLE `campo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campo->usuario` (`usuario_id`) USING BTREE,
  ADD KEY `cooperativa_id` (`cooperativa_id`),
  ADD KEY `cooperativa_id_2` (`cooperativa_id`);

--
-- Indices de la tabla `cooperativa`
--
ALTER TABLE `cooperativa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gasto`
--
ALTER TABLE `gasto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gasto->servicio` (`servicio_id`),
  ADD KEY `gasto->trabajador` (`trabajador_id`),
  ADD KEY `cooperativa_id` (`cooperativa_id`);

--
-- Indices de la tabla `noticia`
--
ALTER TABLE `noticia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cooperativa_id` (`cooperativa_id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cooperativa_id` (`cooperativa_id`);

--
-- Indices de la tabla `producto_usuario`
--
ALTER TABLE `producto_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `producto_usuario->usuario` (`usuario_id`) USING BTREE,
  ADD KEY `producto_usuario->producto` (`producto_id`) USING BTREE,
  ADD KEY `producto_usuario->temporada` (`temporada_id`) USING BTREE,
  ADD KEY `cooperativa_id` (`cooperativa_id`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cooperativa_id` (`cooperativa_id`);

--
-- Indices de la tabla `servicio_campo`
--
ALTER TABLE `servicio_campo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `servicio_campo->servicio` (`servicio_id`) USING BTREE,
  ADD KEY `servicio_campo->temporada` (`temporada_id`) USING BTREE,
  ADD KEY `servicio_campo->campo` (`campo_id`) USING BTREE,
  ADD KEY `servicio_campo->trabajador` (`trabajador_id`),
  ADD KEY `cooperativa_id` (`cooperativa_id`);

--
-- Indices de la tabla `temporada`
--
ALTER TABLE `temporada`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cooperativa_id` (`cooperativa_id`);

--
-- Indices de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cooperativa_id` (`cooperativa_id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mail` (`mail`),
  ADD KEY `cooperativa_id` (`cooperativa_id`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `venta->producto` (`producto_id`),
  ADD KEY `venta->temporada` (`temporada_id`),
  ADD KEY `cooperativa_id` (`cooperativa_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `campo`
--
ALTER TABLE `campo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `gasto`
--
ALTER TABLE `gasto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `noticia`
--
ALTER TABLE `noticia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `producto_usuario`
--
ALTER TABLE `producto_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `servicio_campo`
--
ALTER TABLE `servicio_campo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `temporada`
--
ALTER TABLE `temporada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `campo`
--
ALTER TABLE `campo`
  ADD CONSTRAINT `campo_ibfk_1` FOREIGN KEY (`cooperativa_id`) REFERENCES `cooperativa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `campo_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `gasto`
--
ALTER TABLE `gasto`
  ADD CONSTRAINT `gasto->servicio_campo` FOREIGN KEY (`servicio_id`) REFERENCES `servicio_campo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `gasto->trabajador` FOREIGN KEY (`trabajador_id`) REFERENCES `trabajador` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `gasto_ibfk_1` FOREIGN KEY (`cooperativa_id`) REFERENCES `cooperativa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `noticia`
--
ALTER TABLE `noticia`
  ADD CONSTRAINT `noticia_ibfk_1` FOREIGN KEY (`cooperativa_id`) REFERENCES `cooperativa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`cooperativa_id`) REFERENCES `cooperativa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto_usuario`
--
ALTER TABLE `producto_usuario`
  ADD CONSTRAINT `producto_usuario-producto` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_usuario-temporada` FOREIGN KEY (`temporada_id`) REFERENCES `temporada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_usuario-usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_usuario_ibfk_1` FOREIGN KEY (`cooperativa_id`) REFERENCES `cooperativa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD CONSTRAINT `servicio_ibfk_1` FOREIGN KEY (`cooperativa_id`) REFERENCES `cooperativa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `servicio_campo`
--
ALTER TABLE `servicio_campo`
  ADD CONSTRAINT `servicio_campo->trabajador` FOREIGN KEY (`trabajador_id`) REFERENCES `trabajador` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `servicio_campo-campo` FOREIGN KEY (`campo_id`) REFERENCES `campo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `servicio_campo-servicio` FOREIGN KEY (`servicio_id`) REFERENCES `servicio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `servicio_campo-temporada` FOREIGN KEY (`temporada_id`) REFERENCES `temporada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `servicio_campo_ibfk_1` FOREIGN KEY (`cooperativa_id`) REFERENCES `cooperativa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `temporada`
--
ALTER TABLE `temporada`
  ADD CONSTRAINT `temporada_ibfk_1` FOREIGN KEY (`cooperativa_id`) REFERENCES `cooperativa` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD CONSTRAINT `trabajador_ibfk_1` FOREIGN KEY (`cooperativa_id`) REFERENCES `cooperativa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`cooperativa_id`) REFERENCES `cooperativa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `venta->producto` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `venta->temporada` FOREIGN KEY (`temporada_id`) REFERENCES `temporada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`cooperativa_id`) REFERENCES `cooperativa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
